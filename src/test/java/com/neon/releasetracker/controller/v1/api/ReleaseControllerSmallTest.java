package com.neon.releasetracker.controller.v1.api;

import com.neon.releasetracker.controller.v1.model.Release;
import com.neon.releasetracker.controller.v1.model.ReleaseFilter;
import com.neon.releasetracker.controller.v1.model.ReleaseRequest;
import com.neon.releasetracker.controller.v1.model.Status;
import com.neon.releasetracker.service.ReleaseService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 16:36
 */
@RunWith(MockitoJUnitRunner.class)
public class ReleaseControllerSmallTest {

    @InjectMocks
    private ReleaseController controller;

    @Mock
    private ReleaseService service;

    @Test
    public void client_get_resource_successful() throws Throwable {
        // Given
        Mockito.when(service.getRelease(ArgumentMatchers.eq(Fixture.id))).thenReturn(Fixture.release);

        // When
        ResponseEntity<Release> responseEntity = controller.getRelease(Fixture.id);

        // Then
        Assert.assertEquals(Fixture.id, responseEntity.getBody().getId());
    }

    @Test
    public void client_get_resources_successful() {
        // Given
        Mockito.when(service.getReleases(ArgumentMatchers.eq(Fixture.releaseFilter)))
                .thenReturn(Arrays.asList(Fixture.release));

        // When
        ResponseEntity<List<Release>> responseEntity = controller.getReleases(Fixture.releaseFilter);

        // Then
        Assert.assertEquals(1, responseEntity.getBody().size());
    }

    @Test
    public void client_add_resource_successful() {
        // Given
        Mockito.when(service.addRelease(ArgumentMatchers.eq(Fixture.releaseRequest))).thenReturn(Fixture.release);

        // When
        ResponseEntity<Release> responseEntity = controller.addRelease(Fixture.releaseRequest);

        // Then
        Assert.assertEquals(Fixture.id, responseEntity.getBody().getId());
    }

    @Test
    public void client_update_resource_successful() {
        // When
        ResponseEntity<Release> responseEntity = controller.updateRelease(Fixture.releaseRequest);

        // Then
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void client_delete_resource_successful() {
        // When
        ResponseEntity<Release> responseEntity = controller.deleteRelease(Fixture.id);

        // Then
        Mockito.verify(service, Mockito.times(1)).deleteRelease(ArgumentMatchers.eq(Fixture.id));
    }

    private static class Fixture {
        static Long id = 1l;
        static String name = "name";
        static String description = "description";

        static Release release = new Release(id, name, description, Status.IN_DEVELOPMENT,
                LocalDate.now(),
                LocalDateTime.now(),
                LocalDateTime.now());
        static ReleaseFilter releaseFilter = new ReleaseFilter(name, Status.IN_DEVELOPMENT, LocalDate.now(),
                LocalDateTime.now(), LocalDateTime.now());
        static ReleaseRequest releaseRequest = new ReleaseRequest(id, name, description,
                Status.IN_DEVELOPMENT,
                LocalDate.now());
    }

}
