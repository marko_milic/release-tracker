package com.neon.releasetracker.controller.v1.model;

import java.time.LocalDate;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 13:46
 */
public class ReleaseRequest {
    private final Long id;
    private final String name;
    private final String description;
    private final Status status;
    private final LocalDate releaseDate;

    public ReleaseRequest(Long id, String name, String description, Status status, LocalDate releaseDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Status getStatus() {
        return status;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public Long getId() {
        return id;
    }
}
