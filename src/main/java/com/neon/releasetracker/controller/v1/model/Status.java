package com.neon.releasetracker.controller.v1.model;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 11:16
 */
public enum Status {
    CREATED,
    IN_DEVELOPMENT,
    ON_DEV,
    QA_DONE_ON_DEV,
    ON_STAGING,
    QA_DONE_ON_STAGING,
    ON_PROD,
    DONE
}
