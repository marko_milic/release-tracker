package com.neon.releasetracker.controller.v1.model;

import com.neon.releasetracker.service.ReleaseAuditListener;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 11:15
 */
@Entity
@Table(name = Release.TABLE_NAME)
@EntityListeners(ReleaseAuditListener.class)
public class Release extends BaseModel {
    public static final String TABLE_NAME = "releases";
    public static final String NAME_COLUMN = "name";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String STATUS_COLUMN = "status";
    public static final String RELEASE_DATE_COLUMN = "release_date";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = NAME_COLUMN, nullable = false)
    private String name;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @Column(name = STATUS_COLUMN, length = 32)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = RELEASE_DATE_COLUMN, nullable = false)
    private LocalDate releaseDate;

    public Release() {
    }

    public Release(Long id, String name, String description, Status status, LocalDate releaseDate,
            LocalDateTime createdAt, LocalDateTime lastUpdateAt) {
        super(createdAt, lastUpdateAt);
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.releaseDate = releaseDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }
}
