package com.neon.releasetracker.controller.v1.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 11:41
 */
public class ReleaseFilter {
    private final String name;
    private final Status status;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private final LocalDate releaseDate;
    private final LocalDateTime createdAt;
    private final LocalDateTime lastUpdateAt;

    public ReleaseFilter(String name, Status status, LocalDate releaseDate, LocalDateTime createdAt,
            LocalDateTime lastUpdateAt) {
        this.name = name;
        this.status = status;
        this.releaseDate = releaseDate;
        this.createdAt = createdAt;
        this.lastUpdateAt = lastUpdateAt;
    }

    public String getName() {
        return name;
    }

    public Status getStatus() {
        return status;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getLastUpdateAt() {
        return lastUpdateAt;
    }
}
