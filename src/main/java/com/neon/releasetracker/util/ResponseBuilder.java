package com.neon.releasetracker.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 11:30
 */
public class ResponseBuilder {

    public static <T> ResponseEntity<T> created(T body) {
        return of(HttpStatus.CREATED, body);
    }

    public static <T> ResponseEntity<T> badRequest() {
        return of(HttpStatus.BAD_REQUEST).build();
    }

    public static <T> ResponseEntity<T> ok() {
        return of(HttpStatus.OK).build();
    }

    public static <T> ResponseEntity<T> ok(T body) {
        return of(HttpStatus.OK, body);
    }

    public static <T> ResponseEntity<T> notFound() {
        return of(HttpStatus.NOT_FOUND).build();
    }

    public static <T> ResponseEntity<T> of(HttpStatus status, T body) {
        return of(status).body(body);
    }

    public static ResponseEntity.BodyBuilder of(HttpStatus status) {
        return ResponseEntity.status(status);
    }

}
