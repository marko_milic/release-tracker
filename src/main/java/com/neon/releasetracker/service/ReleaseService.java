package com.neon.releasetracker.service;

import com.neon.releasetracker.controller.v1.model.Release;
import com.neon.releasetracker.controller.v1.model.ReleaseFilter;
import com.neon.releasetracker.controller.v1.model.ReleaseRequest;
import com.neon.releasetracker.exception.ResourceNotFoundException;
import com.neon.releasetracker.map.ReleaseRequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 12:49
 */
@Service
public class ReleaseService {

    private final ReleaseRepository releaseRepository;

    @Autowired
    public ReleaseService(ReleaseRepository releaseRepository) {
        this.releaseRepository = releaseRepository;
    }

    public List<Release> getReleases(ReleaseFilter releaseFilter) {
        ReleaseSpecification releaseSpecification = new ReleaseSpecification(releaseFilter);

        List<Release> releases = releaseRepository.findAll(releaseSpecification);

        return releases;
    }

    public Release getRelease(Long id) {
        return releaseRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Release addRelease(ReleaseRequest releaseRequest) {
        return releaseRepository.save(ReleaseRequestMapper.map(releaseRequest));
    }

    public void updateRelease(ReleaseRequest releaseRequest) {
        Release release = releaseRepository.findById(releaseRequest.getId())
                .orElseThrow(ResourceNotFoundException::new);
        ReleaseRequestMapper.updateReleaseFrom(release, releaseRequest);

        releaseRepository.save(release);
    }

    public void deleteRelease(Long id) {
        if (!releaseRepository.existsById(id))
            throw new ResourceNotFoundException();

        releaseRepository.deleteById(id);
    }
}
