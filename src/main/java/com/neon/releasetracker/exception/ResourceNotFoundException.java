package com.neon.releasetracker.exception;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 13:28
 */
public class ResourceNotFoundException extends RuntimeException {
}
